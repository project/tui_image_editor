<?php

namespace Drupal\tui_image_editor\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the TOAST UI Image Editor Block.
 *
 * @Block(
 *   id="tui_image_editor",
 *   admin_label = @Translation("TOAST UI Image Editor Block"),
 * )
 */
class TOASTUIImageEditorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'tui_image_editor' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['image_editor_header'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Image-editor header content'),
      '#description' => $this->t('Image-editor header content'),
      '#default_value' => $config['image_editor_header'] ?? '',
      '#format' => $config['image_editor_header_format'] ?? 'basic_html',
    ];

    $form['image_editor_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Image editor configuration'),
      '#description' => $this->t('With the UI that is the perfect combination of simple and beautiful, the Image Editor is a Full-Featured one that is perfect for everyday use.<br>Flush all caches if the changes are not showing on the front end.'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['image_editor_configuration']['image_editor_theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Select the editor theme'),
      '#options' => [
        'blackTheme' => $this->t('Black Theme'),
        'whiteTheme' => $this->t('White Theme'),
        'customTheme' => $this->t('Custom Theme'),
      ],
      '#default_value' => $config['image_editor_theme'] ?? 'blackTheme',
    ];

    // Get theme attibutes.
    $tui_image_editor_custom_theme_attributes = tui_image_editor_custom_theme_attributes();
    foreach ($tui_image_editor_custom_theme_attributes as $attr_key => $attr_value) {
      $type = $attr_value['type'];

      switch ($type) {
        case 'textfield':
          $title = $attr_value['title'];
          $description = $attr_value['description'];
          $form['image_editor_configuration'][$attr_key] = [
            '#type' => 'textfield',
            '#title' => $this->t('@title', ['@title' => $title]),
            '#default_value' => $config[$attr_key] ?? $attr_value['default_value'],
            '#attributes' => ['class' => ['major-ticks-width']],
            '#states' => [
              'visible' => [
                ':input[name="settings[image_editor_configuration][image_editor_theme]"]' => ['value' => 'customTheme'],
              ],
            ],
            '#description' => $this->t('@description', ['@description' => $description]),
          ];
          break;

        case 'color':
          $title = $attr_value['title'];
          $description = $attr_value['description'];
          $form['image_editor_configuration'][$attr_key] = [
            '#type' => 'color',
            '#title' => $this->t('@title', ['@title' => $title]),
            '#default_value' => $config[$attr_key] ?? $attr_value['default_value'],
            '#attributes' => ['class' => ['major-ticks-width']],
            '#states' => [
              'visible' => [
                ':input[name="settings[image_editor_configuration][image_editor_theme]"]' => ['value' => 'customTheme'],
              ],
            ],
            '#description' => $this->t('@description', ['@description' => $description]),
          ];
          break;
      }
    }

    $form['image_editor_configuration']['menu_bar_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu bar position'),
      '#description' => $this->t('Editor menu bar position'),
      '#options' => [
        'left' => $this->t('Left'),
        'top' => $this->t('Top'),
        'right' => $this->t('Right'),
        'bottom' => $this->t('Bottom'),
      ],
      '#default_value' => $config['menu_bar_position'] ?? 'left',
    ];

    $form['image_editor_configuration']['created_at'] = [
      '#type' => 'textfield',
      '#default_value' => $config['created_at'] ?? time(),
      '#access' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['image_editor_configuration']) {
      $image_editor_configuration = $values['image_editor_configuration'];
      $theme = $image_editor_configuration['image_editor_theme'];
      $this->configuration['image_editor_theme'] = $theme;
      $this->configuration['menu_bar_position'] = $image_editor_configuration['menu_bar_position'];
      $this->configuration['created_at'] = $image_editor_configuration['created_at'];

      if ($theme == 'customTheme') {
        // Get custom theme attributes.
        $tui_image_editor_custom_theme_attributes = tui_image_editor_custom_theme_attributes();

        foreach ($tui_image_editor_custom_theme_attributes as $attr_key => $attr_value) {
          if (isset($image_editor_configuration[$attr_key])) {
            if ($image_editor_configuration[$attr_key] != '') {
              $attr_data = $image_editor_configuration[$attr_key];
            }
            else {
              $attr_data = $attr_value['default_value'];
            }
            $this->configuration[$attr_key] = $attr_data;
          }
        }
      }
    }

    $this->configuration['image_editor_header'] = $values['image_editor_header']['value'] ?? '';
    $this->configuration['image_editor_header_format'] = $values['image_editor_header']['format'] ?? 'basic_html';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $image_editor_theme = ($config['image_editor_theme']) ?? 'blackTheme';
    $menu_bar_position = ($config['menu_bar_position']) ?? 'left';
    $image_editor_header = $config['image_editor_header'] ?? '';
    $blockId = $config['created_at'] ?? time();

    $image_editor_config = [
      'image_editor_theme' => $image_editor_theme,
      'menu_bar_position' => $menu_bar_position,
      'blockId' => $blockId,
    ];

    $theme_attr_data = [];

    if ($image_editor_theme == 'customTheme') {
      // Get custom theme attributes.
      $tui_image_editor_custom_theme_attributes = tui_image_editor_custom_theme_attributes();
      foreach ($tui_image_editor_custom_theme_attributes as $attr_key => $attr_value) {
        if (isset($config[$attr_key])) {
          if ($config[$attr_key] != '') {
            $attr_data = $config[$attr_key];
          }
          else {
            $attr_data = $attr_value['default_value'];
          }
        }
        $theme_attr_data[$attr_key] = $attr_data;
      }
      $image_editor_config['theme_attr_data'] = json_encode($theme_attr_data);
    }

    $build = [];
    $build['image_editor'] = [
      '#theme' => 'tui_image_editor',
      '#image_editor_config' => $image_editor_config,
      '#image_editor_header' => check_markup($image_editor_header, $config['image_editor_header_format']),
    ];

    $build['#attached']['library'][] = 'tui_image_editor/tui_image_editor';
    return $build;
  }

}
