(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.image_editor = {
        attach: function (context, settings) {
            $(once('image_editor', context)).each(function (element) {
                var blackTheme = {
                    'common.bi.image': 'https://uicdn.toast.com/toastui/img/tui-image-editor-bi.png',
                    'common.bisize.width': '251px',
                    'common.bisize.height': '21px',
                    'common.backgroundImage': 'none',
                    'common.backgroundColor': '#1e1e1e',
                    'common.border': '0px',

                    'header.backgroundImage': 'none',
                    'header.backgroundColor': 'transparent',
                    'header.border': '0px',

                    'loadButton.backgroundColor': '#fff',
                    'loadButton.border': '1px solid #ddd',
                    'loadButton.color': '#222',
                    'loadButton.fontFamily': "'Noto Sans', sans-serif",
                    'loadButton.fontSize': '12px',

                    'downloadButton.backgroundColor': '#fdba3b',
                    'downloadButton.border': '1px solid #fdba3b',
                    'downloadButton.color': '#fff',
                    'downloadButton.fontFamily': "'Noto Sans', sans-serif",
                    'downloadButton.fontSize': '12px',

                    'menu.normalIcon.color': '#8a8a8a',
                    'menu.activeIcon.color': '#555555',
                    'menu.disabledIcon.color': '#434343',
                    'menu.hoverIcon.color': '#e9e9e9',
                    'menu.iconSize.width': '24px',
                    'menu.iconSize.height': '24px',

                    'submenu.normalIcon.color': '#8a8a8a',
                    'submenu.activeIcon.color': '#e9e9e9',
                    'submenu.iconSize.width': '32px',
                    'submenu.iconSize.height': '32px',

                    'submenu.backgroundColor': '#1e1e1e',
                    'submenu.partition.color': '#3c3c3c',

                    'submenu.normalLabel.color': '#8a8a8a',
                    'submenu.normalLabel.fontWeight': 'lighter',
                    'submenu.activeLabel.color': '#fff',
                    'submenu.activeLabel.fontWeight': 'lighter',

                    'checkbox.border': '0px',
                    'checkbox.backgroundColor': '#fff',

                    'range.pointer.color': '#fff',
                    'range.bar.color': '#666',
                    'range.subbar.color': '#d1d1d1',

                    'range.disabledPointer.color': '#414141',
                    'range.disabledBar.color': '#282828',
                    'range.disabledSubbar.color': '#414141',

                    'range.value.color': '#fff',
                    'range.value.fontWeight': 'lighter',
                    'range.value.fontSize': '11px',
                    'range.value.border': '1px solid #353535',
                    'range.value.backgroundColor': '#151515',
                    'range.title.color': '#fff',
                    'range.title.fontWeight': 'lighter',

                    'colorpicker.button.border': '1px solid #1e1e1e',
                    'colorpicker.title.color': '#fff',
                };

                var whiteTheme = {
                    'common.bi.image': 'https://uicdn.toast.com/toastui/img/tui-image-editor-bi.png',
                    'common.bisize.width': '251px',
                    'common.bisize.height': '21px',
                    'common.backgroundImage': './img/bg.png',
                    'common.backgroundColor': '#fff',
                    'common.border': '1px solid #c1c1c1',

                    'header.backgroundImage': 'none',
                    'header.backgroundColor': 'transparent',
                    'header.border': '0px',

                    'loadButton.backgroundColor': '#fff',
                    'loadButton.border': '1px solid #ddd',
                    'loadButton.color': '#222',
                    'loadButton.fontFamily': "'Noto Sans', sans-serif",
                    'loadButton.fontSize': '12px',

                    'downloadButton.backgroundColor': '#fdba3b',
                    'downloadButton.border': '1px solid #fdba3b',
                    'downloadButton.color': '#fff',
                    'downloadButton.fontFamily': "'Noto Sans', sans-serif",
                    'downloadButton.fontSize': '12px',

                    'menu.normalIcon.color': '#8a8a8a',
                    'menu.activeIcon.color': '#555555',
                    'menu.disabledIcon.color': '#434343',
                    'menu.hoverIcon.color': '#e9e9e9',
                    'menu.iconSize.width': '24px',
                    'menu.iconSize.height': '24px',

                    'submenu.normalIcon.color': '#8a8a8a',
                    'submenu.activeIcon.color': '#555555',
                    'submenu.iconSize.width': '32px',
                    'submenu.iconSize.height': '32px',

                    'submenu.backgroundColor': 'transparent',
                    'submenu.partition.color': '#e5e5e5',

                    'submenu.normalLabel.color': '#858585',
                    'submenu.normalLabel.fontWeight': 'normal',
                    'submenu.activeLabel.color': '#000',
                    'submenu.activeLabel.fontWeight': 'normal',

                    'checkbox.border': '1px solid #ccc',
                    'checkbox.backgroundColor': '#fff',

                    'range.pointer.color': '#333',
                    'range.bar.color': '#ccc',
                    'range.subbar.color': '#606060',

                    'range.disabledPointer.color': '#d3d3d3',
                    'range.disabledBar.color': 'rgba(85,85,85,0.06)',
                    'range.disabledSubbar.color': 'rgba(51,51,51,0.2)',

                    'range.value.color': '#000',
                    'range.value.fontWeight': 'normal',
                    'range.value.fontSize': '11px',
                    'range.value.border': '0',
                    'range.value.backgroundColor': '#f5f5f5',
                    'range.title.color': '#000',
                    'range.title.fontWeight': 'lighter',

                    'colorpicker.button.border': '0px',
                    'colorpicker.title.color': '#000',
                };

                $('.tui-image-editor-container').each(function () {
                    var editorData = $(this);
                    var thisId = editorData.attr('id');
                    var theme = editorData.data('theme');
                    if (theme == 'whiteTheme') {
                        theme = whiteTheme;
                    } else if (theme == 'blackTheme') {
                        theme = blackTheme;
                    } else {
                        if (jQuery(this)[0].hasAttribute('data-theme-attr-data')) {
                            var config = $(this).attr('data-theme-attr-data');
                            var obj = $.parseJSON(config);
                            theme = {
                                'common.bi.image': (obj['common_bi_image'] != 'none') ? obj['common_bi_image'] : '',
                                'common.bisize.width': obj['common_bisize_width'],
                                'common.bisize.height': obj['common_bisize_height'],
                                'common.backgroundImage': obj['common_backgroundImage'],
                                'common.backgroundColor': obj['common_backgroundColor'],
                                'common.border': obj['common_border'],

                                'header.backgroundImage': obj['header_backgroundImage'],
                                'header.backgroundColor': obj['header_backgroundColor'],
                                'header.border': obj['header_border'],

                                'loadButton.backgroundColor': obj['loadButton_backgroundColor'],
                                'loadButton.border': obj['loadButton_border'],
                                'loadButton.color': obj['loadButton_color'],
                                'loadButton.fontFamily': obj['loadButton_fontFamily'],
                                'loadButton.fontSize': obj['loadButton_fontSize'],

                                'downloadButton.backgroundColor': obj['downloadButton_backgroundColor'],
                                'downloadButton.border': obj['downloadButton_border'],
                                'downloadButton.color': obj['downloadButton_color'],
                                'downloadButton.fontFamily': obj['downloadButton_fontFamily'],
                                'downloadButton.fontSize': obj['downloadButton_fontSize'],

                                'menu.normalIcon.color': obj['menu_normalIcon_color'],
                                'menu.activeIcon.color': obj['menu_activeIcon_color'],
                                'menu.disabledIcon.color': obj['menu_disabledIcon_color'],
                                'menu.hoverIcon.color': obj['menu_hoverIcon_color'],
                                'menu.iconSize.width': obj['menu_iconSize_width'],
                                'menu.iconSize.height': obj['menu_iconSize_height'],

                                'submenu.normalIcon.color': obj['submenu_normalIcon_color'],
                                'submenu.activeIcon.color': obj['submenu_activeIcon_color'],
                                'submenu.iconSize.width': obj['submenu_iconSize_width'],
                                'submenu.iconSize.height': obj['submenu_iconSize_height'],

                                'submenu.backgroundColor': obj['submenu_backgroundColor'],
                                'submenu.partition.color': obj['submenu_partition_color'],

                                'submenu.normalLabel.color': obj['submenu_normalLabel_color'],
                                'submenu.normalLabel.fontWeight': obj['submenu_normalLabel_fontWeight'],
                                'submenu.activeLabel.color': obj['submenu_activeLabel_color'],
                                'submenu.activeLabel.fontWeight': obj['submenu_activeLabel_fontWeight'],

                                'checkbox.border': obj['checkbox_border'],
                                'checkbox.backgroundColor': obj['checkbox_backgroundColor'],

                                'range.pointer.color': obj['range_pointer_color'],
                                'range.bar.color': obj['range_bar_color'],
                                'range.subbar.color': obj['range_subbar_color'],

                                'range.disabledPointer.color': obj['range_disabledPointer_color'],
                                'range.disabledBar.color': obj['range_disabledBar_color'],
                                'range.disabledSubbar.color': obj['range_disabledSubbar_color'],

                                'range.value.color': obj['range_value_color'],
                                'range.value.fontWeight': obj['range_value_fontWeight'],
                                'range.value.fontSize': obj['range_value_fontSize'],
                                'range.value.border': obj['range_value_border'],
                                'range.value.backgroundColor': obj['range_value_backgroundColor'],
                                'range.title.color': obj['range_title_color'],
                                'range.title.fontWeight': obj['range_title_fontWeight'],

                                'colorpicker.button.border': obj['colorpicker_button_border'],
                                'colorpicker.title.color': obj['colorpicker_title_color'],
                            };
                        }
                    }
                    var menuBarPosition = editorData.data('menu-bar-position');

                    var imageEditor = new tui.ImageEditor('#' + thisId, {
                        includeUI: {
                            loadImage: {
                                path: 'img/sampleImage2.png',
                                name: 'SampleImage'
                            },
                            theme: theme, //whiteTheme
                            initMenu: 'filter',
                            menuBarPosition: menuBarPosition
                        },
                        cssMaxWidth: 700,
                        cssMaxHeight: 900,
                        usageStatistics: 'FALSE',

                    });
                    window.onresize = function () {
                        imageEditor.ui.resizeEditor();
                    }
                });

                $('.tui-image-editor-header-logo img').each(function () {
                    var logoData = $(this);
                    if (!logoData.attr('src')) {
                        logoData.remove();
                    }
                });

            });
        }
    }
}(jQuery, Drupal, drupalSettings));
